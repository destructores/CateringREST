USE [BD_CateringV0]
GO
/****** Object:  Table [dbo].[t_catering]    Script Date: 10/08/2016 14:25:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_catering](
	[tx_codigo] [nvarchar](12) NOT NULL,
	[tx_detalle] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_t_catering] PRIMARY KEY CLUSTERED 
(
	[tx_codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
