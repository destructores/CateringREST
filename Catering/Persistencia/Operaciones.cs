﻿using Catering.Dominio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Messaging;
using System.Web;

namespace Catering.Persistencia
{
    public class Operaciones
    {
        public static void EncolarConfirmacion(string codigo, string detalle)
        {
            string rutaCola = @".\private$\OutputReservas";
            if (!MessageQueue.Exists(rutaCola))
                MessageQueue.Create(rutaCola);
            MessageQueue cola = new MessageQueue(rutaCola);
            Message mensaje = new Message();
            mensaje.Label = "MatriculaReserva";
            mensaje.Body = new Servicio() { Codigo = codigo, Detalle = detalle };
            cola.Send(mensaje);
        }

        public static string EncolarBDOffline(string detalle)
        {
            string codigoAutogenerado = null;
            string rutaCola = @".\private$\InputCatering";
            if (!MessageQueue.Exists(rutaCola))
                MessageQueue.Create(rutaCola);
            MessageQueue cola = new MessageQueue(rutaCola);
            Message mensaje = new Message();
            mensaje.Label = "ErrorRegistro";
            ServicioDAO dao = new ServicioDAO();
            codigoAutogenerado = dao.GeneraCodigo();
            mensaje.Body = new Servicio() { Codigo = codigoAutogenerado, Detalle = detalle };
            cola.Send(mensaje);
            return codigoAutogenerado;
        }


    }
}