﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Catering.Dominio;

namespace Catering.Persistencia
{
    public class ServicioDAO
    {
        public string Crear(string detalle)
        {
            string codigoGenerado = "";
            string sql = "INSERT INTO t_catering VALUES (@codigo, @detalle)";
            using (SqlConnection con = new SqlConnection(ConexionUtil.Cadena))
            {
                con.Open();
                using (SqlCommand com = new SqlCommand(sql, con))
                {
                    codigoGenerado = GeneraCodigo();
                    com.Parameters.Add(new SqlParameter("@codigo", codigoGenerado));
                    com.Parameters.Add(new SqlParameter("@detalle", detalle));
                    com.ExecuteNonQuery();
                }
            }
            return codigoGenerado;
        }

        public string GeneraCodigo() 
        {
            DateTime fechaHora = DateTime.Now;
            string codigoGenerado = fechaHora.ToString("yyMMddHHmmss");
            return codigoGenerado;
        }


        public void RegistrarCola(Servicio servicioARegistrar)
        {
            string sql = "INSERT INTO t_catering VALUES (@codigo, @detalle)";
            using (SqlConnection con = new SqlConnection(ConexionUtil.Cadena))
            {
                con.Open();
                using (SqlCommand com = new SqlCommand(sql, con))
                {
                    com.Parameters.Add(new SqlParameter("@codigo", servicioARegistrar.Codigo));
                    com.Parameters.Add(new SqlParameter("@detalle", servicioARegistrar.Detalle));
                    com.ExecuteNonQuery();
                }
            }
        }

        public Servicio Obtener(string codigo)
        {
            Servicio servicioEncontrado = null;
            string sql = "SELECT * FROM t_catering WHERE tx_codigo = @codigo";
            using (SqlConnection con = new SqlConnection(ConexionUtil.Cadena))
            {
                con.Open();
                using (SqlCommand com = new SqlCommand(sql, con))
                {
                    com.Parameters.Add(new SqlParameter("@codigo", codigo));
                    using (SqlDataReader resultado = com.ExecuteReader())
                    {
                        if (resultado.Read())
                        {
                            servicioEncontrado = new Servicio()
                            {
                                Codigo = (string)resultado["tx_codigo"],
                                Detalle = (string)resultado["tx_detalle"]
                            };
                        }
                    }
                }
            }
            return servicioEncontrado;
        }

        public List<Servicio> Listar()
        {
            List<Servicio> serviciosEncontrados = new List<Servicio>();
            Servicio servicioEncontrado = null;
            string sql = "Select * From t_catering";
            using (SqlConnection con = new SqlConnection(ConexionUtil.Cadena))
            {
                con.Open();
                using (SqlCommand com = new SqlCommand(sql, con))
                {
                    using (SqlDataReader resultado = com.ExecuteReader())
                    {
                        while (resultado.Read())
                        {
                            servicioEncontrado = new Servicio()
                            {
                                Codigo = (string)resultado["tx_codigo"],
                                Detalle = (string)resultado["tx_detalle"]
                            };
                            serviciosEncontrados.Add(servicioEncontrado);
                        }
                    }
                }
            }
            return serviciosEncontrados;
        }
    }
}