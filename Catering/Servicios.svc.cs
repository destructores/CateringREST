﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Catering.Dominio;
using Catering.Persistencia;
using System.Messaging;

namespace Catering
{
    public class Servicios : IServicios
    {
        private ServicioDAO dao = new ServicioDAO();

        public string CrearServicio(string detalle)
        {
            string codigoAutogenerado = "";
            try
            {
                codigoAutogenerado = dao.Crear(detalle);
                // COLA PARA ERICK ver Operaciones.cs
                Operaciones.EncolarConfirmacion(codigoAutogenerado, detalle);
            }
            catch
            {
                // Envio a cola en caso de BDOffline
                string code = Operaciones.EncolarBDOffline(detalle);
                codigoAutogenerado = code;
                return codigoAutogenerado;
            }
            return codigoAutogenerado;
        }

        public Servicio ObtenerServicio(string codigo)
        {
            return dao.Obtener(codigo);
        }

        public List<Servicio> ListarServicios()
        {
            string rutaCola = @".\private$\InputCatering";
            if (!MessageQueue.Exists(rutaCola))
                MessageQueue.Create(rutaCola);
            MessageQueue cola = new MessageQueue(rutaCola);
            cola.Formatter = new XmlMessageFormatter(new Type[] { typeof(Servicio) });
            int numeroMensajes = cola.GetAllMessages().Count();
            int contador = 0;
            while (contador < numeroMensajes) {
                Message mensaje = cola.Receive();
                Servicio servicioEncolado = (Servicio)mensaje.Body;
                contador = contador + 1;
                dao.RegistrarCola(servicioEncolado);
                // COLA PARA ERICK
                string rutaCola2 = @".\private$\OutputReservas";
                if (!MessageQueue.Exists(rutaCola2))
                    MessageQueue.Create(rutaCola2);
                MessageQueue cola2 = new MessageQueue(rutaCola2);
                Message mensaje2 = new Message();
                mensaje2.Label = "MatriculaReserva";
                mensaje2.Body = new Servicio() { Codigo = servicioEncolado.Codigo, Detalle = servicioEncolado.Detalle };
                cola2.Send(mensaje2);
            }
            return dao.Listar();
        }
    }
}
