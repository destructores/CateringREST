﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Catering.Dominio;
using System.ServiceModel.Web;

namespace Catering
{
    [ServiceContract]
    public interface IServicios
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Servicios", ResponseFormat = WebMessageFormat.Json)]
        string CrearServicio(string detalle);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Servicios/{codigo}", ResponseFormat = WebMessageFormat.Json)]
        Servicio ObtenerServicio(string codigo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Servicios", ResponseFormat = WebMessageFormat.Json)]
        List<Servicio> ListarServicios();
    }
}
