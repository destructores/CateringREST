﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Catering.Dominio
{
        [DataContract]
        public class Servicio
        {
            [DataMember]
        	public string Codigo { get; set; }
        	[DataMember]
        	public string Detalle { get; set; }
        }
}